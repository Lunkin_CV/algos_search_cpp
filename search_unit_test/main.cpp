#include <iostream>
#include "array_sequence.hpp"
//#include "search.hpp"
#include "task.hpp"
#include <vector>

#define ARR_SIZE 14

int main(int argc, const char * argv[]) {
    int correct = 0;
    ArraySequence<int>* test_arr_seq;
    int simple[ARR_SIZE] = {2, 4, 10, 10, 10, 12, 14, 16, 18, 20, 22, 22, 22, 24};
    
    
    try {
        cout << "#################### Tests for Binary Search. ####################\n";
        
        
        cout << "SEARCH TESTS.\n";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < ARR_SIZE; i++) {
            (*test_arr_seq).Append(simple[i]);
        }
        
        //1
        cout << "1. Element is in the first half. ";
        if (BinarySearch(test_arr_seq, 4) != 1) {
            cout << "NOT PASSED\n";
            throw runtime_error("1. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        //2
        cout << "2. Element is in the second half. ";
        if (BinarySearch(test_arr_seq, 20) != 9) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        //3
        cout << "3. Element is in the middle. ";
        if (BinarySearch(test_arr_seq, 14) != 6) {
            cout << "NOT PASSED\n";
            throw runtime_error("3. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        //4
        cout << "4. Element is first. ";
        if (BinarySearch(test_arr_seq, 2) != 0) {
            cout << "NOT PASSED\n";
            throw runtime_error("4. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        //5
        cout << "5. Element is last. ";
        if (BinarySearch(test_arr_seq, 24) != 13) {
            cout << "NOT PASSED\n";
            throw runtime_error("5. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        //6
        cout << "6. Element is less than first. ";
        if (BinarySearch(test_arr_seq, 0) != -1) {
            cout << "NOT PASSED\n";
            throw runtime_error("6. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        //7
        cout << "7. Element is bigger than last. ";
        if (BinarySearch(test_arr_seq, 26) != -1) {
            cout << "NOT PASSED\n";
            throw runtime_error("7. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        //8
        cout << "8. Element is not in the sequence, but must be in it. ";
        if (BinarySearch(test_arr_seq, 13) != -1) {
            cout << "NOT PASSED\n";
            throw runtime_error("8. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        delete test_arr_seq;
        
        
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        (*test_arr_seq).Append(1);
        
        //9
        cout << "9. One-element sequence, element is there. ";
        if (BinarySearch(test_arr_seq, 1) != 0) {
            cout << "NOT PASSED\n";
            throw runtime_error("9. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        //10
        cout << "10. One-element sequence, element is not there, it's bigger. ";
        if (BinarySearch(test_arr_seq, 5) != -1) {
            cout << "NOT PASSED\n";
            throw runtime_error("10. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        //11
        cout << "11. One-element sequence, element is not there, it's less. ";
        if (BinarySearch(test_arr_seq, 1) != 0) {
            cout << "NOT PASSED\n";
            throw runtime_error("11. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        
        
        cout << "\nEXCP TESTS.\n";
        //12
        cout << "12. NULL sequence pointer. ";
        try {
            correct = 0;
            BinarySearch<ArraySequence<int>, int>(nullptr, 0);
        } catch (const std::invalid_argument& excp) {
            correct = 1;
        }
        if (correct == 0) {
            cout << "NOT PASSED\n";
            throw runtime_error("12. BubbleSort is wrong.");
        }
        cout << "PASSED\n";
        
        //13
        cout << "13. Division by thero test. ";
        try {
            correct = 0;
            BinarySearch<ArraySequence<int>, int>(test_arr_seq, 0, 1, 0);
        } catch (const std::invalid_argument& excp) {
            correct = 1;
        }
        if (correct == 0) {
            cout << "NOT PASSED\n";
            throw runtime_error("13. BinarySearch is wrong.");
        }
        cout << "PASSED\n";
        
        delete test_arr_seq;
        
        cout << "\n\n#################### Tests for NodeInfo. ####################\n";
        NodeInfo<int, long long int>* test_node_info;
        
        //1
        cout << "1. Constructor test. Key & Value. ";
        try {
            test_node_info = new NodeInfo<int, long long int>(1, 2);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        if (test_node_info->GetKey() != 1 ||
            test_node_info->GetValue() != 2) {
            cout << "NOT PASSED\n";
            throw runtime_error("1. NodeInfo constructor is wrong.");
        }
        cout << "PASSED\n";
        delete test_node_info;
        
        //2
        cout << "2. Constructor test. Key only. ";
        try {
            test_node_info = new NodeInfo<int, long long int>(1);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        if (test_node_info->GetKey() != 1 ||
            test_node_info->GetValue() != 1) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. NodeInfo constructor is wrong.");
        }
        cout << "PASSED\n";
        delete test_node_info;
        
        //3
        cout << "3. Operators == != > < >= <= test. ";
        try {
            test_node_info = new NodeInfo<int, long long int>(1, 2);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        if (!(*test_node_info == NodeInfo<int, long long int>(1) &&
              *test_node_info != NodeInfo<int, long long int>(2) &&
              *test_node_info > NodeInfo<int, long long int>(0) &&
              *test_node_info < NodeInfo<int, long long int>(3) &&
              *test_node_info <= NodeInfo<int, long long int>(1) &&
              *test_node_info >= NodeInfo<int, long long int>(1) &&
              *test_node_info <= NodeInfo<int, long long int>(3) &&
              *test_node_info >= NodeInfo<int, long long int>(0))) {
            cout << "NOT PASSED\n";
            throw runtime_error("3. NodeInfo operators are wrong.");
        }
        cout << "PASSED\n";
        delete test_node_info;
        
        cout << "\n\n#################### Tests for BinaryTree. ####################\n";
        BinaryTree<NodeInfo<int, long long int>>* test_tree;
        struct TreeNode<NodeInfo<int, long long int>>* test_tree_node;
        
        //1
        cout << "1. Constructor test. ";
        try {
            test_tree = new BinaryTree<NodeInfo<int, long long int>>(NodeInfo<int, long long int>(1, 2));
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        test_tree_node = (*test_tree).GetRootPtr();
        if (test_tree_node->info_.GetKey() != 1 ||
            test_tree_node->info_.GetValue() != 2 ||
            test_tree_node->index_ != 0 ||
            test_tree_node->left_ != nullptr ||
            test_tree_node->right_ != nullptr) {
            cout << "NOT PASSED\n";
            throw runtime_error("1. BinaryTree constructor is wrong.");
        }
        cout << "PASSED\n";
        delete test_tree;
        
        //2
        cout << "2. Sorted sequence builder and insert test. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < ARR_SIZE; i++) {
            (*test_arr_seq).Append(simple[i]);
        }
        
        test_tree = BuildTreeSeq<BinaryTree<NodeInfo<int, long long int>>, ArraySequence<int>, int>(test_arr_seq);
        test_tree_node = (*test_tree).GetRootPtr();
        for (int i = 0; i < (*test_arr_seq).GetLength() - 1; i++) {
            if (test_tree_node->info_.GetKey() != (*test_arr_seq).Get(i) ||
                test_tree_node->info_.GetValue() != i ||
                test_tree_node->left_ != nullptr) {
                cout << "NOT PASSED\n";
                throw runtime_error("2. BuildTree is wrong.");
            }
            test_tree_node = test_tree_node->right_;
        }
        if (test_tree_node->info_.GetKey() != (*test_arr_seq).GetLast() ||
            test_tree_node->info_.GetValue() != (*test_arr_seq).GetLength() - 1 ||
            test_tree_node->left_ != nullptr ||
            test_tree_node->right_ != nullptr) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. BuildTree is wrong.");
        }
        cout << "PASSED\n";
        delete test_tree;
        delete test_arr_seq;
        
        //3
        cout << "3. 50, 25, 25, 10, 1, 100 Add test. ";
        try {
            test_tree = new BinaryTree<NodeInfo<int, long long int>>();
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        (*test_tree).Add(NodeInfo<int, long long int>(50));
        (*test_tree).Add(NodeInfo<int, long long int>(25));
        (*test_tree).Add(NodeInfo<int, long long int>(25));
        (*test_tree).Add(NodeInfo<int, long long int>(10));
        (*test_tree).Add(NodeInfo<int, long long int>(1));
        (*test_tree).Add(NodeInfo<int, long long int>(100));
        test_tree_node = (*test_tree).GetRootPtr();
        if (test_tree_node->info_.GetKey() != 50 ||
            test_tree_node->info_.GetValue() != 50 ||
            test_tree_node->left_ == nullptr ||
            test_tree_node->right_ == nullptr) {
            cout << "NOT PASSED\n";
            throw runtime_error("3. Add is wrong.");
        }
        if (test_tree_node->right_->info_.GetKey() != 100 ||
            test_tree_node->right_->info_.GetValue() != 100 ||
            test_tree_node->right_->left_ != nullptr ||
            test_tree_node->right_->right_ != nullptr) {
            cout << "NOT PASSED\n";
            throw runtime_error("3. Add is wrong.");
        }
        test_tree_node = test_tree_node->left_;
        if (test_tree_node->info_.GetKey() != 25 ||
            test_tree_node->info_.GetValue() != 25 ||
            test_tree_node->left_ == nullptr ||
            test_tree_node->right_ == nullptr) {
            cout << "NOT PASSED\n";
            throw runtime_error("3. Add is wrong.");
        }
        if (test_tree_node->right_->info_.GetKey() != 25 ||
            test_tree_node->right_->info_.GetValue() != 25 ||
            test_tree_node->right_->left_ != nullptr ||
            test_tree_node->right_->right_ != nullptr) {
            cout << "NOT PASSED\n";
            throw runtime_error("3. Add is wrong.");
        }
        test_tree_node = test_tree_node->left_;
        if (test_tree_node->info_.GetKey() != 10 ||
            test_tree_node->info_.GetValue() != 10 ||
            test_tree_node->left_ == nullptr ||
            test_tree_node->right_ != nullptr) {
            cout << "NOT PASSED\n";
            throw runtime_error("3. Add is wrong.");
        }
        test_tree_node = test_tree_node->left_;
        if (test_tree_node->info_.GetKey() != 1 ||
            test_tree_node->info_.GetValue() != 1 ||
            test_tree_node->left_ != nullptr ||
            test_tree_node->right_ != nullptr) {
            cout << "NOT PASSED\n";
            throw runtime_error("3. Add is wrong.");
        }
        cout << "PASSED\n";
        
        //4
        cout << "4. 50, 25, 25, 10, 1, 100 IndexOf existing test. ";
        if (test_tree->IndexOf(NodeInfo<int, long long int>(1)) < 0) {
            cout << "NOT PASSED\n";
            throw runtime_error("4. IndexOf is wrong.");
        }
        cout << "PASSED\n";
        
        //5
        cout << "5. 50, 25, 25, 10, 1, 100 TryGet nonexisting test. ";
        if (test_tree->IndexOf(NodeInfo<int, long long int>(2)) != -1) {
            cout << "NOT PASSED\n";
            throw runtime_error("5. IndexOf is wrong.");
        }
        cout << "PASSED\n";
        
        //6
        cout << "5. 50, 25, 25, 10, 1, 100 indexes check. ";
        if (test_tree->GetLength() != 6 ||
            test_tree->Get(0).GetKey() != 1 ||
            test_tree->Get(1).GetKey() != 10 ||
            test_tree->Get(2).GetKey() != 25 ||
            test_tree->Get(3).GetKey() != 25 ||
            test_tree->Get(4).GetKey() != 50 ||
            test_tree->Get(5).GetKey() != 100) {
            cout << "NOT PASSED\n";
            throw runtime_error("6. indexes are wrong.");
        }
        cout << "PASSED\n";
        
        //7
        cout << "5. 50, 25, 25, 10, 1, 100: Remove upper 25 and indexes check. ";
        test_tree->Remove(2);
        if (test_tree->GetLength() != 5 ||
            test_tree->Get(0).GetKey() != 1 ||
            test_tree->Get(1).GetKey() != 10 ||
            test_tree->Get(2).GetKey() != 25 ||
            test_tree->Get(3).GetKey() != 50 ||
            test_tree->Get(4).GetKey() != 100) {
            cout << "NOT PASSED\n";
            throw runtime_error("7. Remove was wrong.");
        }
        cout << "PASSED\n";
        
        delete test_tree;
        
        cout << "\nEXCP TESTS.\n";
        
        //8
        cout << "8. NULL sequence pointer. ";
        try {
            correct = 0;
            test_tree = BuildTreeSeq<BinaryTree<NodeInfo<int, long long int>>, ArraySequence<int>, int>(nullptr);
        } catch (const std::invalid_argument& excp) {
            correct = 1;
        }
        if (correct == 0) {
            cout << "NOT PASSED\n";
            throw runtime_error("8. BuildTree is wrong.");
        }
        cout << "PASSED\n";
        
        
        
        cout << "\n\n#################### Tests for AVLBinaryTree. ####################\n";
        AVLBinaryTree<NodeInfo<int, long long int>>* test_tree_avl;
        struct AVLTreeNode<NodeInfo<int, long long int>>* test_tree_node_avl;
        
        //1
        cout << "1. Constructor test. ";
        try {
            test_tree_avl = new AVLBinaryTree<NodeInfo<int, long long int>>(NodeInfo<int, long long int>(1, 2));
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        test_tree_node_avl = (*test_tree_avl).GetRootPtr();
        if (test_tree_node_avl->info_.GetKey() != 1 ||
            test_tree_node_avl->info_.GetValue() != 2 ||
            test_tree_node_avl->index_ != 0 ||
            test_tree_node_avl->left_ != nullptr ||
            test_tree_node_avl->right_ != nullptr) {
            cout << "NOT PASSED\n";
            throw runtime_error("1. BinaryTree constructor is wrong.");
        }
        cout << "PASSED\n";
        delete test_tree_avl;
        
        //2
        cout << "2. Sorted sequence builder and insert test. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < ARR_SIZE; i++) {
            (*test_arr_seq).Append(simple[i]);
        }
        
        test_tree_avl = BuildTreeSeq<AVLBinaryTree<NodeInfo<int, long long int>>, ArraySequence<int>, int>(test_arr_seq);
        test_tree_node_avl = (*test_tree_avl).GetRootPtr();
        
        if (test_tree_avl->Check() == false) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. BuildAVLTree is wrong.");
        }
        
        for (int i = 0; i < test_arr_seq->GetLength(); i++) {
            if (test_tree_avl->Get(i).GetValue() != i ||
                test_tree_avl->Get(i).GetKey() != test_arr_seq->Get(i)) {
                cout << "NOT PASSED\n";
                throw runtime_error("2. BuildAVLTree is wrong.");
            }
        }
        cout << "PASSED\n";
        
        //3
        cout << "3. Remove tests. Indexes to remove 2, 2, 5, 10.";
        std::vector<int> test_int_vector;
        for (int i = 0; i < test_arr_seq->GetLength(); i++) {
            test_int_vector.push_back(test_arr_seq->Get(i));
        }
        if (test_tree_avl->Check() == false) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. BuildAVLTree is wrong.");
        }
        
        test_tree_avl->Remove(2);
        test_int_vector.erase(test_int_vector.begin() + 2);
        if (test_tree_avl->Check() == false) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. Remove is wrong 1.1.");
        }
        for (int i = 0; i < test_int_vector.size(); i++) {
            
            if (test_tree_avl->Get(i).GetKey() != test_int_vector[i]) {
                cout << "NOT PASSED\n";
                throw runtime_error("2. Remove is wrong 1.2.");
            }
        }
        
        test_tree_avl->Remove(2);
        test_int_vector.erase(test_int_vector.begin() + 2);
        if (test_tree_avl->Check() == false) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. Remove is wrong 2.1.");
        }
        for (int i = 0; i < test_int_vector.size(); i++) {
            if (test_tree_avl->Get(i).GetKey() != test_int_vector[i]) {
                cout << "NOT PASSED\n";
                throw runtime_error("2. Remove is wrong 2.2.");
            }
        }
        
        test_tree_avl->Remove(5);
        test_int_vector.erase(test_int_vector.begin() + 5);
        if (test_tree_avl->Check() == false) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. Remove is wrong 3.1.");
        }
        for (int i = 0; i < test_int_vector.size(); i++) {
            if (test_tree_avl->Get(i).GetKey() != test_int_vector[i]) {
                cout << "NOT PASSED\n";
                throw runtime_error("2. Remove is wrong 3.2.");
            }
        }
        
        test_tree_avl->Remove(10);
        test_int_vector.erase(test_int_vector.begin() + 10);
        if (test_tree_avl->Check() == false) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. Remove is wrong 4.1.");
        }
        for (int i = 0; i < test_int_vector.size(); i++) {
            if (test_tree_avl->Get(i).GetKey() != test_int_vector[i]) {
                cout << "NOT PASSED\n";
                throw runtime_error("2. Remove is wrong 4.2.");
            }
        }
        cout << "PASSED\n";
        
        delete test_tree_avl;
        
        cout << "\n\n#################### Tests for Tasks. ####################\n";
        
        SearchTask<int>* test_task;
        try {
            test_task = new SearchTask<int>(test_arr_seq, BINARY | GBINARY | TREE | AVL | PBINARY);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        
        //1
        cout << "Search of existing element with index 6.\n";
        vector<double> rel;
        rel.push_back(1.5);
        rel.push_back(0.5);
        test_task->Search(14, true, nullptr, rel);
        
        //2
        cout << "Search of non-existing element.\n";
        test_task->Search(-5, true, nullptr, rel);
        
        
        delete test_arr_seq;
        
        
        
        cout << "\nALL TESTS ARE PASSED\n";
    } catch (const runtime_error& re) {
        cout << "Runtime error: " << re.what() << endl;
    } catch (const exception& ex) {
        cout << "Error occurred: " << ex.what() << endl;
    } catch (...) {
        cout << "Unknown failure occurred. Possible memory corruption" << endl;
    }
    return 0;
}
