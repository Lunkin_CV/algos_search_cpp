#ifndef task_hpp
#define task_hpp

#include <stdio.h>
#include "search.hpp"
#include "array_sequence.hpp"
#include "list_sequence.hpp"
#include "sorts.hpp"
#include "compare.hpp"

//IDs of methods
#define BINARY 1
#define GBINARY 2
#define PBINARY 4
#define TREE 8
#define AVL 16
#define MAX_METHOD 16

template <typename TKey>
class SearchTask {
public:
    SearchTask(ArraySequence<TKey>* raw, int methods); //from existing sequence
    SearchTask(int methods); //from user
    SearchTask(long long int ammount, int gen_mode, int methods); //generate
    SearchTask(long long int ammount, int methods); //generate random
    ~SearchTask();
    void Search(TKey key, bool check, ostream* str, vector<double> rel);
    void RandomSearch(bool exist);
    void PrintRaw(ostream* str);
    int GetMethods();
private:
    void Prepare();
    
    ArraySequence<TKey>* raw_data_ = nullptr;
    
    ArraySequence<NodeInfo<TKey, long long int>>* arr_seq_ = nullptr;
    ListSequence<NodeInfo<TKey, long long int>>* list_seq_ = nullptr;
    BinaryTree<NodeInfo<TKey, long long int>>* tree_ = nullptr;
    AVLBinaryTree<NodeInfo<TKey, long long int>>* avl_tree_ = nullptr;
    
    double arr_seq_prep_time_ = -1;
    double list_seq_prep_time_ = -1;
    double tree_prep_time_ = -1;
    double avl_tree_prep_time_ = -1;
    
    int gen_mode_ = 0;
    int methods_ = 0;
    bool prepared_ = false;
    
};

template <typename TKey>
int SearchTask<TKey>::GetMethods() {
    return methods_;
}

template <typename TKey>
void SearchTask<TKey>::PrintRaw(ostream* str) {
    if (str != nullptr && prepared_ == true) {
        for (long long int i = 0; i < raw_data_->GetLength(); i++) {
            *str << raw_data_->Get(i) << ";";
        }
        *str << "\n";
    }
}



template <typename TKey>
void SearchTask<TKey>::Prepare() {
    clock_t begin, end;
    if (raw_data_->GetIsEmpty() == 1 || prepared_ == true) {
        return;
    }
    
    if ((methods_ & (BINARY | GBINARY | PBINARY)) != 0) {
        begin = clock();
        for (long long int i = 0; i < raw_data_->GetLength(); i++) {
            arr_seq_->Append(NodeInfo<TKey, long long int>(raw_data_->Get(i),i));
        }
        HeapSort(arr_seq_, 0, 0);
        end = clock();
        arr_seq_prep_time_ = double(end - begin) / CLOCKS_PER_SEC;
        
        begin = clock();
        for (long long int i = 0; i < raw_data_->GetLength(); i++) {
            list_seq_->Append(NodeInfo<TKey, long long int>(raw_data_->Get(i),i));
        }
        HeapSort(list_seq_, 0, 0);
        end = clock();
        list_seq_prep_time_ = double(end - begin) / CLOCKS_PER_SEC;
    }
    
    if ((methods_ & TREE) != 0) {
        begin = clock();
        for (long long int i = 0; i < raw_data_->GetLength(); i++) {
            tree_->Add(NodeInfo<TKey, long long int>(raw_data_->Get(i),i));
        }
        end = clock();
        tree_prep_time_ = double(end - begin) / CLOCKS_PER_SEC;
    }
    
    if ((methods_ & AVL) != 0) {
        begin = clock();
        for (long long int i = 0; i < raw_data_->GetLength(); i++) {
            avl_tree_->Add(NodeInfo<TKey, long long int>(raw_data_->Get(i),i));
        }
        end = clock();
        avl_tree_prep_time_ = double(end - begin) / CLOCKS_PER_SEC;
    }
    
    prepared_ = true;
}

template <typename TKey>
SearchTask<TKey>::SearchTask(ArraySequence<TKey>* raw, int methods) {
    if (methods <= 0 || methods >= MAX_METHOD * 2) {
        string er = "invalid methods in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (raw == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (raw->GetIsEmpty() == 1) {
        string er = "raw sequence is empty in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    try {
        raw_data_ = new ArraySequence<TKey>();
        arr_seq_ = new ArraySequence<NodeInfo<TKey, long long int>>();
        list_seq_ = new ListSequence<NodeInfo<TKey, long long int>>();
        tree_ = new BinaryTree<NodeInfo<TKey, long long int>>();
        avl_tree_ = new AVLBinaryTree<NodeInfo<TKey, long long int>>();
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    for (long long int i = 0; i < raw->GetLength(); i++) {
        raw_data_->Append(raw->Get(i));
    }
    methods_ = methods;
    Prepare();
}

template<typename TKey>
SearchTask<TKey>::SearchTask(int methods) {
    if (methods <= 0 || methods >= MAX_METHOD * 2) {
        cout << methods;
        string er = "invalid methods in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    cout << "Now start to input sequence.\nAfter each new member press Enter.\nWhen you finish, type \"end\" and press Enter.\nMembers can be from " << INT_MIN << " to " << INT_MAX << ".\n";
    try {
        raw_data_ = new ArraySequence<TKey>();
        arr_seq_ = new ArraySequence<NodeInfo<TKey, long long int>>();
        list_seq_ = new ListSequence<NodeInfo<TKey, long long int>>();
        tree_ = new BinaryTree<NodeInfo<TKey, long long int>>();
        avl_tree_ = new AVLBinaryTree<NodeInfo<TKey, long long int>>();
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    string line;
    cout << ">";
    while (getline(cin, line)) {
        if (line.compare("end") == 0) {
            break;
        }
        raw_data_->Append(stoi(line));
        cout << ">";
    }
    methods_ = methods;
    Prepare();
}

template<typename TKey>
SearchTask<TKey>::SearchTask(long long int ammount, int mode, int methods) {
    if (methods <= 0 || methods >= MAX_METHOD * 2) {
        string er = "invalid methods in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (ammount <= 0) {
        string er = "ammount is zero or less in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    try {
        raw_data_ = new ArraySequence<TKey>();
        arr_seq_ = new ArraySequence<NodeInfo<TKey, long long int>>();
        list_seq_ = new ListSequence<NodeInfo<TKey, long long int>>();
        tree_ = new BinaryTree<NodeInfo<TKey, long long int>>();
        avl_tree_ = new AVLBinaryTree<NodeInfo<TKey, long long int>>();
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    FillInt(raw_data_, ammount, mode);
    methods_ = methods;
    Prepare();
    if (mode > MAX_MODE || mode <= 0) {
        gen_mode_ = RANDOM;
    } else {
        gen_mode_ = mode;
    }
}

template<typename TKey>
SearchTask<TKey>::SearchTask(long long int ammount, int methods) {
    if (methods <= 0 || methods >= MAX_METHOD * 2) {
        string er = "invalid methods in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (ammount <= 0) {
        string er = "ammount is zero or less in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    try {
        raw_data_ = new ArraySequence<TKey>();
        arr_seq_ = new ArraySequence<NodeInfo<TKey, long long int>>();
        list_seq_ = new ListSequence<NodeInfo<TKey, long long int>>();
        tree_ = new BinaryTree<NodeInfo<TKey, long long int>>();
        avl_tree_ = new AVLBinaryTree<NodeInfo<TKey, long long int>>();
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    FillInt(raw_data_, ammount, RANDOM);
    methods_ = methods;
    Prepare();
    gen_mode_ = RANDOM;
}

template <typename TKey>
SearchTask<TKey>::~SearchTask() {
    if (raw_data_ != nullptr) {
        delete raw_data_;
    }
    if (arr_seq_ != nullptr) {
        delete arr_seq_;
    }
    if (list_seq_ != nullptr) {
        delete list_seq_;
    }
    if (tree_ != nullptr) {
        delete tree_;
    }
    if (avl_tree_ != nullptr) {
        delete avl_tree_;
    }
}

template <typename TKey>
void SearchTask<TKey>::Search(TKey key, bool check, ostream* str, vector<double> rel) {
    if (prepared_ == false) {
        cout << "Task is unprepared.\n";
        return;
    }
    for (int i = 0; i < rel.size(); i++) {
        if (rel[i] < 0) {
            string er = "negative proportion in ";
            er += __PRETTY_FUNCTION__;
            throw invalid_argument(er);
        }
    }
    
    long long int index_result;
    double time_result;
    clock_t begin, end;
    
    
    
    cout << "#################### Search element " << key << " ####################\n";
    if (str != nullptr) {
        *str << "#################### Search element " << key << " ####################\n";
    }
    
    if ((methods_ & TREE) != 0) {
        begin = clock();
        index_result = tree_->IndexOf(NodeInfo<TKey, long long int>(key));
        end = clock();
        time_result = double(end - begin) / CLOCKS_PER_SEC;
        
        if (check == true) {
            if (index_result == -1) {
                for (long long int i = 0; i < raw_data_->GetLength(); i++) {
                    if (raw_data_->Get(i) == key) {
                        string er = "invalid tree search in ";
                        er += __PRETTY_FUNCTION__;
                        throw runtime_error(er);
                    }
                }
            } else if (tree_->Get(index_result) != key) {
                string er = "invalid tree search in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
        }
        
        cout << "Search in tree.\n" << "Result: ";
        if (index_result == -1) {
            cout << "not found.\n";
        } else {
            cout << index_result << " in tree. " << tree_->Get(index_result).GetValue() << " in raw sequence.\n";
        }
        if (check == true) {
            cout << "Search is correct.\n";
        }
        cout << "Preparation time: " << tree_prep_time_ << " seconds.\n";
        cout << "Search time: " << time_result << " seconds.\n";
        cout << "Total time: " << tree_prep_time_ + time_result << " seconds.\n";
        cout << "\n";
        
        if (str != nullptr) {
            *str << "Search in tree.\n" << "Result: ";
            if (index_result == -1) {
                *str << "not found.\n";
            } else {
                *str << index_result << " in tree. " << tree_->Get(index_result).GetValue() << " in raw sequence.\n";
            }
            if (check == true) {
                *str << "Search is correct.\n";
            }
            *str << "Preparation time: " << tree_prep_time_ << " seconds.\n";
            *str << "Search time: " << time_result << " seconds.\n";
            *str << "Total time: " << tree_prep_time_ + time_result << " seconds.\n";
            *str << "\n";
        }
        
    }
    
    if ((methods_ & AVL) != 0) {
        begin = clock();
        index_result = avl_tree_->IndexOf(NodeInfo<TKey, long long int>(key));
        end = clock();
        time_result = double(end - begin) / CLOCKS_PER_SEC;
        
        if (check == true) {
            if (index_result == -1) {
                for (long long int i = 0; i < raw_data_->GetLength(); i++) {
                    if (raw_data_->Get(i) == key) {
                        string er = "invalid avl tree search in ";
                        er += __PRETTY_FUNCTION__;
                        throw runtime_error(er);
                    }
                }
            } else if (avl_tree_->Get(index_result) != key) {
                string er = "invalid avl tree search in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
        }
        
        cout << "Search in AVL tree.\n" << "Result: ";
        if (index_result == -1) {
            cout << "not found.\n";
        } else {
            cout << index_result << " in tree. " << avl_tree_->Get(index_result).GetValue() << " in raw sequence.\n";
        }
        if (check == true) {
            cout << "Search is correct.\n";
        }
        cout << "Preparation time: " << avl_tree_prep_time_ << " seconds.\n";
        cout << "Search time: " << time_result << " seconds.\n";
        cout << "Total time: " << avl_tree_prep_time_ + time_result << " seconds.\n";
        cout << "\n";
        
        if (str != nullptr) {
            *str << "Search in AVL tree.\n" << "Result: ";
            if (index_result == -1) {
                *str << "not found.\n";
            } else {
                *str << index_result << " in tree. " << avl_tree_->Get(index_result).GetValue() << " in raw sequence.\n";
            }
            if (check == true) {
                *str << "Search is correct.\n";
            }
            *str << "Preparation time: " << avl_tree_prep_time_ << " seconds.\n";
            *str << "Search time: " << time_result << " seconds.\n";
            *str << "Total time: " << avl_tree_prep_time_ + time_result << " seconds.\n";
            *str << "\n";
        }
    }
    
    if ((methods_ & BINARY) != 0) {
        begin = clock();
        index_result = BinarySearch(arr_seq_, NodeInfo<TKey, long long int>(key));
        end = clock();
        time_result = double(end - begin) / CLOCKS_PER_SEC;
        if (check == true) {
            if (index_result == -1) {
                for (long long int i = 0; i < raw_data_->GetLength(); i++) {
                    if (raw_data_->Get(i) == key) {
                        string er = "invalid array binary search in ";
                        er += __PRETTY_FUNCTION__;
                        throw runtime_error(er);
                    }
                }
            } else if (arr_seq_->Get(index_result) != key) {
                string er = "invalid array binary search in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
        }
        
        cout << "Standart binary search in sorted array sequence.\n" << "Result: ";
        if (index_result == -1) {
            cout << "not found.\n";
        } else {
            cout << index_result << " in sorted sequence. " << arr_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
        }
        if (check == true) {
            cout << "Search is correct.\n";
        }
        cout << "Preparation time: " << arr_seq_prep_time_ << " seconds.\n";
        cout << "Search time: " << time_result << " seconds.\n";
        cout << "Total time: " << arr_seq_prep_time_ + time_result << " seconds.\n";
        cout << "\n";
        
        if (str != nullptr) {
            *str << "Standart binary search in sorted array sequence.\n" << "Result: ";
            if (index_result == -1) {
                *str << "not found.\n";
            } else {
                *str << index_result << " in sorted sequence. " << arr_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
            }
            if (check == true) {
                *str << "Search is correct.\n";
            }
            *str << "Preparation time: " << arr_seq_prep_time_ << " seconds.\n";
            *str << "Search time: " << time_result << " seconds.\n";
            *str << "Total time: " << arr_seq_prep_time_ + time_result << " seconds.\n";
            *str << "\n";
        }
        
        begin = clock();
        index_result = BinarySearch(list_seq_, NodeInfo<TKey, long long int>(key));
        end = clock();
        time_result = double(end - begin) / CLOCKS_PER_SEC;
        
        if (check == true) {
            if (index_result == -1) {
                for (long long int i = 0; i < raw_data_->GetLength(); i++) {
                    if (raw_data_->Get(i) == key) {
                        string er = "invalid list binary search in ";
                        er += __PRETTY_FUNCTION__;
                        throw runtime_error(er);
                    }
                }
            } else if (list_seq_->Get(index_result) != key) {
                string er = "invalid list binary search in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
        }
        
        cout << "Standart binary search in sorted list sequence.\n" << "Result: ";
        if (index_result == -1) {
            cout << "not found.\n";
        } else {
            cout << index_result << " in sorted sequence. " << list_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
        }
        if (check == true) {
            cout << "Search is correct.\n";
        }
        cout << "Preparation time: " << list_seq_prep_time_ << " seconds.\n";
        cout << "Search time: " << time_result << " seconds.\n";
        cout << "Total time: " << list_seq_prep_time_ + time_result << " seconds.\n";
        cout << "\n";
        
        if (str != nullptr) {
            *str << "Standart binary search in sorted list sequence.\n" << "Result: ";
            if (index_result == -1) {
                *str << "not found.\n";
            } else {
                *str << index_result << " in tree. " << list_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
            }
            if (check == true) {
                *str << "Search is correct.\n";
            }
            *str << "Preparation time: " << list_seq_prep_time_ << " seconds.\n";
            *str << "Search time: " << time_result << " seconds.\n";
            *str << "Total time: " << list_seq_prep_time_ + time_result << " seconds.\n";
            *str << "\n";
        }
    }
    
    if ((methods_ & GBINARY) != 0) {
        begin = clock();
        index_result = BinarySearchGolden(arr_seq_, NodeInfo<TKey, long long int>(key));
        end = clock();
        time_result = double(end - begin) / CLOCKS_PER_SEC;
        
        if (check == true) {
            if (index_result == -1) {
                for (long long int i = 0; i < raw_data_->GetLength(); i++) {
                    if (raw_data_->Get(i) == key) {
                        string er = "invalid array binary search in ";
                        er += __PRETTY_FUNCTION__;
                        throw runtime_error(er);
                    }
                }
            } else if (arr_seq_->Get(index_result) != key) {
                string er = "invalid array binary search in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
        }
        
        cout << "Golden binary search in sorted array sequence.\n" << "Result: ";
        if (index_result == -1) {
            cout << "not found.\n";
        } else {
            cout << index_result << " in sorted sequence. " << arr_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
        }
        if (check == true) {
            cout << "Search is correct.\n";
        }
        cout << "Preparation time: " << arr_seq_prep_time_ << " seconds.\n";
        cout << "Search time: " << time_result << " seconds.\n";
        cout << "Total time: " << arr_seq_prep_time_ + time_result << " seconds.\n";
        cout << "\n";
        
        if (str != nullptr) {
            *str << "Golden binary search in sorted array sequence.\n" << "Result: ";
            if (index_result == -1) {
                *str << "not found.\n";
            } else {
                *str << index_result << " in sorted sequence. " << arr_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
            }
            if (check == true) {
                *str << "Search is correct.\n";
            }
            *str << "Preparation time: " << arr_seq_prep_time_ << " seconds.\n";
            *str << "Search time: " << time_result << " seconds.\n";
            *str << "Total time: " << arr_seq_prep_time_ + time_result << " seconds.\n";
            *str << "\n";
        }
        
        begin = clock();
        index_result = BinarySearchGolden(list_seq_, NodeInfo<TKey, long long int>(key));
        end = clock();
        time_result = double(end - begin) / CLOCKS_PER_SEC;
        
        if (check == true) {
            if (index_result == -1) {
                for (long long int i = 0; i < raw_data_->GetLength(); i++) {
                    if (raw_data_->Get(i) == key) {
                        string er = "invalid list binary search in ";
                        er += __PRETTY_FUNCTION__;
                        throw runtime_error(er);
                    }
                }
            } else if (list_seq_->Get(index_result) != key) {
                string er = "invalid list binary search in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
        }
        
        cout << "Golden binary search in sorted list sequence.\n" << "Result: ";
        if (index_result == -1) {
            cout << "not found.\n";
        } else {
            cout << index_result << " in sorted sequence. " << list_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
        }
        if (check == true) {
            cout << "Search is correct.\n";
        }
        cout << "Preparation time: " << list_seq_prep_time_ << " seconds.\n";
        cout << "Search time: " << time_result << " seconds.\n";
        cout << "Total time: " << list_seq_prep_time_ + time_result << " seconds.\n";
        cout << "\n";
        
        if (str != nullptr) {
            *str << "Golden binary search in sorted list sequence.\n" << "Result: ";
            if (index_result == -1) {
                *str << "not found.\n";
            } else {
                *str << index_result << " in tree. " << list_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
            }
            if (check == true) {
                *str << "Search is correct.\n";
            }
            *str << "Preparation time: " << list_seq_prep_time_ << " seconds.\n";
            *str << "Search time: " << time_result << " seconds.\n";
            *str << "Total time: " << list_seq_prep_time_ + time_result << " seconds.\n";
            *str << "\n";
        }
    }
    
    if ((methods_ & PBINARY) != 0 && rel.size() != 0) {
        for (int j = 0; j < rel.size(); j++) {
            begin = clock();
            index_result = BinarySearch(arr_seq_, NodeInfo<TKey, long long int>(key), rel[j]);
            end = clock();
            time_result = double(end - begin) / CLOCKS_PER_SEC;
            if (check == true) {
                if (index_result == -1) {
                    for (long long int i = 0; i < raw_data_->GetLength(); i++) {
                        if (raw_data_->Get(i) == key) {
                            string er = "invalid array binary search in ";
                            er += __PRETTY_FUNCTION__;
                            throw runtime_error(er);
                        }
                    }
                } else if (arr_seq_->Get(index_result) != key) {
                    string er = "invalid array binary search in ";
                    er += __PRETTY_FUNCTION__;
                    throw runtime_error(er);
                }
            }
            
            
            cout << "Binary search with relation " << rel[j] << " in sorted array sequence.\n" << "Result: ";
            if (index_result == -1) {
                cout << "not found.\n";
            } else {
                cout << index_result << " in sorted sequence. " << arr_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
            }
            if (check == true) {
                cout << "Search is correct.\n";
            }
            cout << "Preparation time: " << arr_seq_prep_time_ << " seconds.\n";
            cout << "Search time: " << time_result << " seconds.\n";
            cout << "Total time: " << arr_seq_prep_time_ + time_result << " seconds.\n";
            cout << "\n";
            
            if (str != nullptr) {
                *str << "Binary search with relation " << rel[j] << " in sorted array sequence.\n" << "Result: ";
                if (index_result == -1) {
                    *str << "not found.\n";
                } else {
                    *str << index_result << " in sorted sequence. " << arr_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
                }
                if (check == true) {
                    *str << "Search is correct.\n";
                }
                *str << "Preparation time: " << arr_seq_prep_time_ << " seconds.\n";
                *str << "Search time: " << time_result << " seconds.\n";
                *str << "Total time: " << arr_seq_prep_time_ + time_result << " seconds.\n";
                *str << "\n";
            }
        }
        
        for (int j = 0; j < rel.size(); j++) {
            begin = clock();
            index_result = BinarySearch(list_seq_, NodeInfo<TKey, long long int>(key), rel[j]);
            end = clock();
            time_result = double(end - begin) / CLOCKS_PER_SEC;
            if (check == true) {
                if (index_result == -1) {
                    for (long long int i = 0; i < raw_data_->GetLength(); i++) {
                        if (raw_data_->Get(i) == key) {
                            string er = "invalid array binary search in ";
                            er += __PRETTY_FUNCTION__;
                            throw runtime_error(er);
                        }
                    }
                } else if (list_seq_->Get(index_result) != key) {
                    string er = "invalid array binary search in ";
                    er += __PRETTY_FUNCTION__;
                    throw runtime_error(er);
                }
            }
            
            cout << "Binary search with relation " << rel[j] << " in sorted list sequence.\n" << "Result: ";
            if (index_result == -1) {
                cout << "not found.\n";
            } else {
                cout << index_result << " in sorted sequence. " << list_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
            }
            if (check == true) {
                cout << "Search is correct.\n";
            }
            cout << "Preparation time: " << list_seq_prep_time_ << " seconds.\n";
            cout << "Search time: " << time_result << " seconds.\n";
            cout << "Total time: " << list_seq_prep_time_ + time_result << " seconds.\n";
            cout << "\n";
            
            if (str != nullptr) {
                *str << "Binary search with relation " << rel[j] << " in sorted list sequence.\n" << "Result: ";
                if (index_result == -1) {
                    *str << "not found.\n";
                } else {
                    *str << index_result << " in tree. " << list_seq_->Get(index_result).GetValue() << " in raw sequence.\n";
                }
                if (check == true) {
                    *str << "Search is correct.\n";
                }
                *str << "Preparation time: " << list_seq_prep_time_ << " seconds.\n";
                *str << "Search time: " << time_result << " seconds.\n";
                *str << "Total time: " << list_seq_prep_time_ + time_result << " seconds.\n";
                *str << "\n";
            }
        }
    }
    
    cout << "#################### Search ended ####################\n";
    if (str != nullptr) {
        *str << "#################### Search ended ####################\n";
    }
}

#endif /* task_hpp */
