#ifndef search_hpp
#define search_hpp

#include <stdio.h>
#include <math.h>
#include <stack>
#include "../../l0_Sequence/l0_Sequence/sequence.hpp"

#define DEBUG_MODE

/*####################### SortedSequence #######################*/
template <typename TElement>
class SortedSequence {
public:
    virtual long long int GetLength() = 0;
    virtual int GetIsEmpty() = 0;
    virtual TElement Get(long long int index) = 0;
    virtual TElement GetFirst() = 0;
    virtual TElement GetLast() = 0;
    virtual long long int IndexOf(TElement elem) = 0;
    virtual void Add(TElement elem) = 0;
private:
    long long int length_ = 0;
};

/*####################### BinarySearch #######################*/

/*!
 * @discussion Binary search with f=a/b partitioning.
 * In case of multiple elems in sequence result is not defined.
 * @return index of found element or -1 if it has not been found.
 */
template <typename TSeq, typename TElement>
long long int BinarySearch(TSeq* seq, TElement elem, double f) {
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (f < 0) {
        string er = "invalid f in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if ((*seq).GetIsEmpty() == 1) {
        return -1;
    }
    
    long long int left = 0;
    long long int right = (*seq).GetLength() - 1;
    long long int middle = right - (long long int)( (right - left) / (1 + f) );
    int cmp_res = 0;
    
    while (right >= left) {
        middle = right - (long long int)( (right - left) / (1 + f) );
        cmp_res = TElCmp(elem, (*seq).Get(middle));
        if (cmp_res == 0) {
            return middle;
        } else if (cmp_res < 0) {
            right = middle - 1;
        } else {
            left = middle + 1;
        }
    }
    return -1;
}

template <typename TSeq, typename TElement>
long long int BinarySearch(TSeq* seq, TElement elem) {
    return BinarySearch(seq, elem, 1);
}

template <typename TSeq, typename TElement>
long long int BinarySearch(TSeq* seq, TElement elem, unsigned int a, unsigned int b) {
    if (b == 0) {
        string er = "b == 0 will cause division by zero in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    double f = a / b;
    return BinarySearch(seq, elem, f);
}

template <typename TSeq, typename TElement>
long long int BinarySearchGolden(TSeq* seq, TElement elem) {
    double f = (1 + sqrt(5)) / 2;
    return BinarySearch(seq, elem, f);
}

/*####################### NodeInfo #######################*/
template <typename TKey, typename TValue>
class NodeInfo {
public:
    NodeInfo(TKey key, TValue value);
    NodeInfo(TKey key);
    NodeInfo() {};
    ~NodeInfo() {};
    TKey GetKey() const;
    TValue GetValue() const;
    bool operator==(const NodeInfo<TKey, TValue>& b);
    bool operator!=(const NodeInfo<TKey, TValue>& b);
    bool operator>(const NodeInfo<TKey, TValue>& b);
    bool operator<(const NodeInfo<TKey, TValue>& b);
    bool operator>=(const NodeInfo<TKey, TValue>& b);
    bool operator<=(const NodeInfo<TKey, TValue>& b);
    void operator=(const NodeInfo<TKey, TValue>& b);
private:
    TKey key_;
    TValue value_;
};

template <typename TKey, typename TValue>
NodeInfo<TKey, TValue>::NodeInfo(TKey key, TValue value) {
    key_ = key;
    value_ = value;
}

/*!
 * @discussion in case of creating by key key_==value_
 */
template <typename TKey, typename TValue>
NodeInfo<TKey, TValue>::NodeInfo(TKey key) {
    key_ = key;
    value_ = key;
}

template <typename TKey, typename TValue>
TKey NodeInfo<TKey, TValue>::GetKey() const {
    return key_;
}

template <typename TKey, typename TValue>
TValue NodeInfo<TKey, TValue>::GetValue() const {
    return value_;
}

/*!
 * @discussion relation operators compare only key_.
 * if (key1_ == key2_ && value1_ != value2_) NodeInfo1 == NodeInfo2
 */
template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator==(const NodeInfo<TKey, TValue>& b) {
    if (key_ == b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator!=(const NodeInfo<TKey, TValue>& b) {
    if (key_ != b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator>(const NodeInfo<TKey, TValue>& b) {
    if (key_ > b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator<(const NodeInfo<TKey, TValue>& b) {
    if (key_ < b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator>=(const NodeInfo<TKey, TValue>& b) {
    if (key_ >= b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator<=(const NodeInfo<TKey, TValue>& b) {
    if (key_ <= b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
void NodeInfo<TKey, TValue>::operator=(const NodeInfo<TKey, TValue>& b) {
    key_ = b.GetKey();
    value_ = b.GetValue();
}


/*####################### Index Correction #######################*/
/*!
 * @discussion increments index of node and its right subtree
 */
template <typename TTreeNode>
void IncrIndex(TTreeNode* tree) {
    if (tree == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    tree->index_++;
    
    std::stack<TTreeNode*> st;
    if (tree->right_ != nullptr) {
        st.push(tree->right_);
    }
    
    TTreeNode* tmp;
    while (st.empty() == 0) {
        tmp = st.top();
        st.pop();
        if (tmp->left_ != nullptr) {
            st.push(tmp->left_);
        }
        if (tmp->right_ != nullptr) {
            st.push(tmp->right_);
        }
        tmp->index_++;
    }
}

/*!
 * @discussion decrements index of node and its right subtree
 */
template <typename TTreeNode>
void DecrIndex(TTreeNode* tree) {
    if (tree == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    tree->index_--;
    
    std::stack<TTreeNode*> st;
    if (tree->right_ != nullptr) {
        st.push(tree->right_);
    }
    
    TTreeNode* tmp;
    while (st.empty() == 0) {
        tmp = st.top();
        st.pop();
        if (tmp->left_ != nullptr) {
            st.push(tmp->left_);
        }
        if (tmp->right_ != nullptr) {
            st.push(tmp->right_);
        }
        tmp->index_--;
    }
}

/*####################### TreeNode #######################*/
template <typename TNodeInfo>
struct TreeNode
{
    TNodeInfo info_;
    long long int index_;
    struct TreeNode<TNodeInfo>* left_;
    struct TreeNode<TNodeInfo>* right_;
};

/*####################### BinaryTree #######################*/
template <typename TElement>
class BinaryTree: public SortedSequence<TElement> {
public:
    BinaryTree(TElement elem);
    BinaryTree() {};
    ~BinaryTree();
    long long int GetLength();
    int GetIsEmpty();
    TElement Get(long long int index);
    long long int IndexOf(TElement elem);
    TElement GetFirst();
    TElement GetLast();
    BinaryTree<TElement> GetSubsequence(long long int start_index, long long int end_index);
    void Add(TElement elem);
    long long int RemoveElem(TElement elem);
    void Remove(long long int del_index);
#ifdef DEBUG_MODE
    TreeNode<TElement>* GetRootPtr();
#endif
private:
    TreeNode<TElement>* root_ = nullptr;
    long long int length_ = 0;
};

template <typename TElement>
BinaryTree<TElement>::BinaryTree(TElement elem) {
    try {
        root_ = new struct TreeNode<TElement>;
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    root_->index_ = 0;
    root_->info_ = elem;
    root_->left_ = root_->right_ = nullptr;
    length_ = 1;
}

template <typename TElement>
BinaryTree<TElement>::~BinaryTree() {
    std::stack<struct TreeNode<TElement>*> st;
    if (root_ != nullptr) {
        st.push(root_);
    }
    
    struct TreeNode<TElement>* tmp;
    while (st.empty() == 0) {
        tmp = st.top();
        st.pop();
        if (tmp->left_ != nullptr) {
            st.push(tmp->left_);
        }
        if (tmp->right_ != nullptr) {
            st.push(tmp->right_);
        }
        delete tmp;
    }
}

template <typename TElement>
long long int BinaryTree<TElement>::GetLength() {
    return length_;
}

template <typename TElement>
int BinaryTree<TElement>::GetIsEmpty() {
    if (length_ == 0) {
        return 1;
    }
    return 0;
}

template <typename TElement>
TElement BinaryTree<TElement>::Get(long long int index) {
    if (index < 0) {
        string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (index >= length_) {
        string er = "index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    
    TreeNode<TElement>* tmp = root_;
    while (tmp->index_ != index) {
        if (index < tmp->index_) {
            tmp = tmp->left_;
        } else {
            tmp = tmp->right_;
        }
        if (tmp == nullptr) {
            string er = "NULL pointer reached in ";
            er += __PRETTY_FUNCTION__;
            throw invalid_argument(er);
        }
    }
    
    return tmp->info_;
}

template <typename TElement>
long long int BinaryTree<TElement>::IndexOf(TElement elem) {
    if (GetIsEmpty() == 1) {
        return -1;
    }
    struct TreeNode<TElement>* tmp = root_;
    while (tmp != nullptr && tmp->info_ != elem) {
        if (elem < tmp->info_) {
            tmp = tmp->left_;
        } else {
            tmp = tmp->right_;
        }
    }
    
    if (tmp == nullptr) {
        return -1;
    }
    
    return tmp->index_;
}

template <typename TElement>
TElement BinaryTree<TElement>::GetFirst() {
    return Get(0);
}

template <typename TElement>
TElement BinaryTree<TElement>::GetLast() {
    return Get(length_ - 1);
}

template <typename TElement>
BinaryTree<TElement> BinaryTree<TElement>::GetSubsequence(long long int start_index,
                                                          long long int end_index) {
    if (start_index < 0 || end_index < 0) {
        string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (start_index > end_index) {
        string er = "start_index < end_index in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (start_index >= length_) {
        string er = "start_index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    long long int correct_end_index = (end_index < length_) ? end_index : (length_ - 1);
    BinaryTree<TElement> tmp_tree;
    for (long long int i = start_index; i <= correct_end_index; i++) {
        tmp_tree.Add(this->Get(i));
    }
    return tmp_tree;
}

template <typename TElement>
void BinaryTree<TElement>::Add(TElement elem) {
    long long int index = 0;
    struct TreeNode<TElement>* tmp = root_;
    struct TreeNode<TElement>** alloc = &root_;
    while (tmp != nullptr) {
        if (elem < tmp->info_) {
            IncrIndex(tmp);
            index = tmp->index_ - 1;
            alloc = &tmp->left_;
            tmp = tmp->left_;
        } else {
            index = tmp->index_ + 1;
            alloc = &tmp->right_;
            tmp = tmp->right_;
        }
    }
    try {
        *alloc = new struct TreeNode<TElement>;
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    (*alloc)->info_ = elem;
    (*alloc)->index_ = index;
    (*alloc)->right_ = (*alloc)->left_ = nullptr;
    length_++;
}

/*!
 * @discussion it will place left subtree instead of removed, right subtree will be added to left
 */
template <typename TElement>
void BinaryTree<TElement>::Remove(long long int del_index) {
    if (del_index < 0) {
        string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (del_index >= length_) {
        string er = "del_index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    struct TreeNode<TElement>* tmp = root_;
    struct TreeNode<TElement>** change = &root_;
    
    while (tmp->index_ != del_index) {
        if (del_index < tmp->index_) {
            DecrIndex(tmp);
            change = &tmp->left_;
            tmp = tmp->left_;
        } else {
            change = &tmp->right_;
            tmp = tmp->right_;
        }
        if (tmp == nullptr) {
            string er = "NULL pointer reached in ";
            er += __PRETTY_FUNCTION__;
            throw invalid_argument(er);
        }
    }
    DecrIndex(tmp);
    *change = tmp->left_;
    struct TreeNode<TElement>* deep_tmp = *change;
    while (deep_tmp != nullptr) {
        if (tmp->right_->index_ < deep_tmp->index_) {
            IncrIndex(deep_tmp);
            change = &deep_tmp->left_;
            deep_tmp = deep_tmp->left_;
        } else {
            change = &deep_tmp->right_;
            deep_tmp = deep_tmp->right_;
        }
    }
    *change = tmp->right_;
    delete tmp;
    length_--;
}

/*!
 * @discussion index of element to delete is undefined
 * @return -1 if elem is not in the tree; index of deleted element in the other case
 */
template <typename TElement>
long long int BinaryTree<TElement>::RemoveElem(TElement elem){
    long long int index = IndexOf(elem);
    if (index >= 0) {
        Remove(index);
    }
    return index;
}

#ifdef DEBUG_MODE
template <typename TElement>
struct TreeNode<TElement>* BinaryTree<TElement>::GetRootPtr(){
    return root_;
}
#endif

/*####################### AVLTreeNode #######################*/
template <typename TNodeInfo>
struct AVLTreeNode {
    TNodeInfo info_;
    long long int index_;
    int balance_;
    struct AVLTreeNode<TNodeInfo>* left_;
    struct AVLTreeNode<TNodeInfo>* right_;
};

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RotateRight(AVLTreeNode<TNodeInfo>* current) {
    AVLTreeNode<TNodeInfo>* pivot = current->left_;
    current->left_ = pivot->right_;
    pivot->right_ = current;
    if (pivot->balance_ == 1) {
        pivot->balance_ = 0;
        current->balance_ = 0;
    } else if (pivot->balance_ == 0){
        pivot->balance_ = -1;
        current->balance_ = 1;
    } else {
        string er = "Unknown balance case in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return pivot;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RotateLeft(AVLTreeNode<TNodeInfo>* current) {
    AVLTreeNode<TNodeInfo>* pivot = current->right_;
    current->right_ = pivot->left_;
    pivot->left_ = current;
    if (pivot->balance_ == -1) {
        pivot->balance_ = 0;
        current->balance_ = 0;
    } else if (pivot->balance_ == 0){
        pivot->balance_ = 1;
        current->balance_ = -1;
    } else {
        string er = "Unknown balance case in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return pivot;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RotateRightBig(AVLTreeNode<TNodeInfo>* current) {
    AVLTreeNode<TNodeInfo>* pivot = current->left_;
    AVLTreeNode<TNodeInfo>* bottom = pivot->right_;
    current->left_ = bottom->right_;
    pivot->right_ = bottom->left_;
    bottom->left_ = pivot;
    bottom->right_ = current;
    if (bottom->balance_ == 0) {
        pivot->balance_ = 0;
        current->balance_ = 0;
    } else if (bottom->balance_ == -1){
        bottom->balance_ = 0;
        pivot->balance_ = 1;
        current->balance_ = 0;
    } else if (bottom->balance_ == 1){
        bottom->balance_ = 0;
        pivot->balance_ = 0;
        current->balance_ = -1;
    } else {
        string er = "Unknown balance case in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return bottom;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RotateLeftBig(AVLTreeNode<TNodeInfo>* current) {
    AVLTreeNode<TNodeInfo>* pivot = current->right_;
    AVLTreeNode<TNodeInfo>* bottom = pivot->left_;
    current->right_ = bottom->left_;
    pivot->left_ = bottom->right_;
    bottom->left_ = current;
    bottom->right_ = pivot;
    if (bottom->balance_ == 0) {
        pivot->balance_ = 0;
        current->balance_ = 0;
    } else if (bottom->balance_ == -1){
        bottom->balance_ = 0;
        pivot->balance_ = 0;
        current->balance_ = 1;
    } else if (bottom->balance_ == 1){
        bottom->balance_ = 0;
        pivot->balance_ = -1;
        current->balance_ = 0;
    } else {
        string er = "Unknown balance case in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return bottom;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* Rebalance(AVLTreeNode<TNodeInfo>* node) {
    if (node->balance_ == 1 || node->balance_ == -1 || node->balance_ == 0) {
        return node;
    } else if (node->balance_ == 2) {
        if (node->left_->balance_ == 1 || node->left_->balance_ == 0) {
            return RotateRight(node);
        } else if (node->left_->balance_ == -1) {
            return RotateRightBig(node);
        }
    } else if (node->balance_ == -2) {
        if (node->right_->balance_ == -1 || node->right_->balance_ == 0) {
            return RotateLeft(node);
        } else if (node->right_->balance_ == 1) {
            return RotateLeftBig(node);
        }
    } else {
        string er = "Unknown balance case in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return node;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* DeleteTree(AVLTreeNode<TNodeInfo>* node) {
    if (node == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (node->left_ != nullptr) {
        DeleteTree(node->left_);
    }
    if (node->right_ != nullptr) {
        DeleteTree(node->right_);
    }
    delete node;
    return nullptr;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* Insert(AVLTreeNode<TNodeInfo>* root, TNodeInfo info, long long int index, bool* need_correction) {
    bool current_need_correction = false;
    AVLTreeNode<TNodeInfo>* new_tree;
    if (root == nullptr) {
        try {
            new_tree = new struct AVLTreeNode<TNodeInfo>;
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        new_tree->info_ = info;
        new_tree->index_ = index;
        new_tree->balance_ = 0;
        new_tree->right_ = new_tree->left_ = nullptr;
        *need_correction = true;
        return new_tree;
    } else if (info < root->info_) {
        IncrIndex(root);
        root->left_ = Insert(root->left_, info, root->index_ - 1, &current_need_correction);
        if (current_need_correction == true) {
            root->balance_++;
        }
        new_tree = Rebalance(root);
        if (new_tree->balance_ == 0 || current_need_correction == false) {
            *need_correction = false;
        } else {
            *need_correction = true;
        }
        return new_tree;
    } else {
        root->right_ = Insert(root->right_, info, root->index_ + 1, &current_need_correction);
        if (current_need_correction == true) {
            root->balance_--;
        }
        new_tree = Rebalance(root);
        if (new_tree->balance_ == 0 || current_need_correction == false) {
            *need_correction = false;
        } else {
            *need_correction = true;
        }
        return new_tree;
    }
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RemoveNode(AVLTreeNode<TNodeInfo>* root, long long int index, bool* need_correction) {
    bool current_need_correction = false;
    AVLTreeNode<TNodeInfo>* new_tree;
    if (index < root->index_) {
        DecrIndex(root);
        root->left_ = RemoveNode(root->left_, index, &current_need_correction);
        if (current_need_correction == true) {
            root->balance_--;
        }
        new_tree = Rebalance(root);
        if (new_tree->balance_ == -1 || current_need_correction == false) {
            *need_correction = false;
        } else {
            *need_correction = true;
        }
        return new_tree;
    } else if (index > root->index_) {
        root->right_ = RemoveNode(root->right_, index, &current_need_correction);
        if (current_need_correction == true) {
            root->balance_++;
        }
        new_tree = Rebalance(root);
        if (new_tree->balance_ == 1 || current_need_correction == false) {
            *need_correction = false;
        } else {
            *need_correction = true;
        }
        return new_tree;
    } else {
        AVLTreeNode<TNodeInfo>* left_sub = root->left_;
        AVLTreeNode<TNodeInfo>* right_sub = root->right_;
        delete root;
        if (right_sub == nullptr) {
            *need_correction = true;
            return left_sub;
        }
        AVLTreeNode<TNodeInfo>* min = FindMin(right_sub);
        min->right_ = RemoveMin(right_sub, &current_need_correction);
        min->left_ = left_sub;
        min->index_--;
        if (current_need_correction == true) {
            min->balance_++;
        }
        new_tree = Rebalance(min);
        if (current_need_correction == false || new_tree->balance_ == 1) {
            *need_correction = false;
        } else {
            *need_correction = true;
        }
        return new_tree;
    }
}

template <typename TNodeInfo>
int GetHeight(AVLTreeNode<TNodeInfo>* root)
{
    if (root == nullptr) {
        return 0;
    } else {
        int right_h = GetHeight(root->right_);
        int left_h = GetHeight(root->left_);
        return 1 + (right_h > left_h ? right_h : left_h);
    }
}

template <typename TNodeInfo>
bool BalanceCheck(AVLTreeNode<TNodeInfo>* root){
    if (root == nullptr) {
        return true;
    } else {
        if (abs(root->balance_) > 1) {
            return false;
        }
        if (GetHeight(root->left_) - GetHeight(root->right_) != root->balance_) {
            return false;
        }
        return true && BalanceCheck(root->right_) && BalanceCheck(root->left_);
    }
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* FindMin(AVLTreeNode<TNodeInfo>* node) {
    if (node == nullptr) {
        return node;
    }
    AVLTreeNode<TNodeInfo>* tmp = node;
    while (tmp->left_ != nullptr) {
        tmp = tmp->left_;
    }
    return tmp;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RemoveMin(AVLTreeNode<TNodeInfo>* node, bool* need_correction) {
    bool current_need_correction = false;
    if(node->left_ == nullptr) {
        *need_correction = true;
        return node->right_;
    }
    
    DecrIndex(node);
    node->left_ = RemoveMin(node->left_, &current_need_correction);
    if (current_need_correction == true) {
        node->balance_--;
    }
    AVLTreeNode<TNodeInfo>* new_tree = Rebalance(node);
    if (current_need_correction == false || new_tree->balance_ == -1) {
        *need_correction = false;
    } else {
        *need_correction = true;
    }
    return new_tree;
}

/*####################### AVLBinaryTree #######################*/
template <typename TElement>
class AVLBinaryTree: public SortedSequence<TElement> {
public:
    AVLBinaryTree(TElement elem);
    AVLBinaryTree() {};
    ~AVLBinaryTree();
    long long int GetLength();
    int GetIsEmpty();
    TElement Get(long long int index);
    long long int IndexOf(TElement elem);
    TElement GetFirst();
    TElement GetLast();
    AVLBinaryTree<TElement> GetSubsequence(long long int start_index, long long int end_index);
    void Add(TElement elem);
    long long int RemoveElem(TElement elem);
    void Remove(long long int del_index);
#ifdef DEBUG_MODE
    bool Check();
    AVLTreeNode<TElement>* GetRootPtr();
#endif
private:
    AVLTreeNode<TElement>* root_ = nullptr;
    long long int length_ = 0;
};

#ifdef DEBUG_MODE
template <typename TElement>
bool AVLBinaryTree<TElement>::Check() {
    return BalanceCheck(root_);
}
#endif

template <typename TElement>
AVLBinaryTree<TElement>::AVLBinaryTree(TElement elem) {
    try {
        root_ = new struct AVLTreeNode<TElement>;
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    root_->index_ = 0;
    root_->info_ = elem;
    root_->balance_ = 0;
    root_->left_ = root_->right_ = nullptr;
    length_ = 1;
}

template <typename TElement>
AVLBinaryTree<TElement>::~AVLBinaryTree() {
    if (!GetIsEmpty()) {
        root_ = DeleteTree(root_);
    }
}

template <typename TElement>
long long int AVLBinaryTree<TElement>::GetLength() {
    return length_;
}

template <typename TElement>
int AVLBinaryTree<TElement>::GetIsEmpty() {
    if (length_ == 0) {
        return 1;
    }
    return 0;
}

template <typename TElement>
TElement AVLBinaryTree<TElement>::Get(long long int index) {
    if (index < 0) {
        string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (index >= length_) {
        string er = "index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    
    AVLTreeNode<TElement>* tmp = root_;
    while (tmp->index_ != index) {
        if (index < tmp->index_) {
            tmp = tmp->left_;
        } else {
            tmp = tmp->right_;
        }
        if (tmp == nullptr) {
            string er = "NULL pointer reached in ";
            er += __PRETTY_FUNCTION__;
            throw invalid_argument(er);
        }
    }
    
    return tmp->info_;
}

template <typename TElement>
long long int AVLBinaryTree<TElement>::IndexOf(TElement elem) {
    if (GetIsEmpty() == 1) {
        return -1;
    }
    struct AVLTreeNode<TElement>* tmp = root_;
    while (tmp != nullptr && tmp->info_ != elem) {
        if (elem < tmp->info_) {
            tmp = tmp->left_;
        } else {
            tmp = tmp->right_;
        }
    }
    
    if (tmp == nullptr) {
        return -1;
    }
    
    return tmp->index_;
}

template <typename TElement>
TElement AVLBinaryTree<TElement>::GetFirst() {
    return Get(0);
}

template <typename TElement>
TElement AVLBinaryTree<TElement>::GetLast() {
    return Get(length_ - 1);
}

template <typename TElement>
AVLBinaryTree<TElement> AVLBinaryTree<TElement>::GetSubsequence(long long int start_index,
                                                          long long int end_index) {
    if (start_index < 0 || end_index < 0) {
        string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (start_index > end_index) {
        string er = "start_index < end_index in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (start_index >= length_) {
        string er = "start_index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    long long int correct_end_index = (end_index < length_) ? end_index : (length_ - 1);
    AVLBinaryTree<TElement> tmp_tree;
    for (long long int i = start_index; i <= correct_end_index; i++) {
        tmp_tree.Add(this->Get(i));
    }
    return tmp_tree;
}

template <typename TElement>
void AVLBinaryTree<TElement>::Add(TElement elem) {
    bool need_correction = false;
    root_ = Insert(root_, elem, 0, &need_correction);
    length_++;
}

/*!
 * @discussion it will place left subtree instead of removed, right subtree will be added to left
 */
template <typename TElement>
void AVLBinaryTree<TElement>::Remove(long long int del_index) {
    if (del_index < 0) {
        string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (del_index >= length_) {
        string er = "del_index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    bool current_need_correction = false;
    root_ = RemoveNode(root_, del_index, &current_need_correction);
    length_--;
}

/*!
 * @discussion index of element to delete is undefined
 * @return -1 if elem is not in the tree; index of deleted element in the other case
 */
template <typename TElement>
long long int AVLBinaryTree<TElement>::RemoveElem(TElement elem){
    long long int index = IndexOf(elem);
    if (index >= 0) {
        Remove(index);
    }
    return index;
}

#ifdef DEBUG_MODE
template <typename TElement>
struct AVLTreeNode<TElement>* AVLBinaryTree<TElement>::GetRootPtr(){
    return root_;
}
#endif

/*####################### Tree Builder #######################*/
template <typename TTree, typename TSeq, typename TKey>
TTree* BuildTreeSeq(TSeq *seq) {
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    TTree* tree;
    try {
        tree = new TTree();
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    
    for (long long int i = 0; i < (*seq).GetLength(); i++) {
        (*tree).Add(NodeInfo<TKey, long long int>((*seq).Get(i), i));
    }
    
    return tree;
}



#endif /* search_hpp */
