#include <iostream>
#include "task.hpp"

#define HELP "#################### OPERATION MANUAL ####################\n\
This program allows you to compare Binary search, Tree search and AVL Tree search on your own sequences of int numbers (from file or keyboard) and generated sequences (random, upward, backward and criss-cross). \n\
It will generate a report with results and, if you want, save raw sequence.\n\n\
There are two types of keys: major keys and minor keys.\n\
Major key tells the program what to do. In each command you must use only one major key.\n\
Minor key adds details. However, sometimes minor keys are required.\n\
So the general form of a command is:\n\
./l2_Search --major-key --minor-key1 param --minor-key2 --param1,param2,param3\n\n\
There are three types of tasks: auto tasks, file tasks, manual tasks. \n\
Binary search operates with sequences based on either arrays or lists.\n\
The program can generate sequence for search by itself (random, upward, backward and criss-cross).\
Required minor keys: --amount\n\
The program can take sequence from first line of .csv file with ';' delimiter. \
Required minor keys: --raw-path\n\
Manual tasks take sequences from user via terminal. No required minor keys.\n\
\n\
List of major keys with their minor keys:\n\
--help Shows this manual.\n\
--auto Search in generated sequence.\n\
--auto-check Search in generated sequence and check the result.\n\
--file Add new file task with sequences based on arrays.\n\
--file-check Add new file task with sequences based on lists.\n\
--manual Add new manual task with sequences based on arrays.\n\
--manual-check Add new manual task with sequences based on lists.\n\
    [--algos alg1,alg2,alg3] List of algorythms to test.\n\
        b - standart binary search\n\
        t - binary tree search\n\
        a - binary avl tree search\n\
        p - binary search with specific relation\n\
        g - binary search with golden ratio\n\
        Default: b,t,a,p,g\n\
    {--raw-path PATH} Path to file with sequence.\n\
    [--report-path PATH] Path to file for report.\n\
    [--mode mode] Only for generated sequence. Mode for sequence generation.\n\
        r - random\n\
        u - upward\n\
        b - backward\n\
        c - criss-cross\n\
        Default: r\n\
    --amount am1 Only for generated sequence. Amounts for sequnce generation.\n\
"

void ArgRec(int argc, const char* argv[], int* types, ofstream* report_file, long long int* amount, int* mode, ArraySequence<int>* seq){
    for (int i = 2; i < argc - 1; i++) {
        if (strcmp(argv[i], "--report-path") == 0) {
            if (i + 1 < argc) {
                (*report_file).open(argv[i + 1], ios::out | ios::ate);
            }
        } else if (strcmp(argv[i], "--algos") == 0) {
            if (strstr(argv[i + 1], "b") != nullptr) {
                *types |= BINARY;
            }
            if (strstr(argv[i + 1], "t") != nullptr) {
                *types |= TREE;
            }
            if (strstr(argv[i + 1], "a") != nullptr) {
                *types |= AVL;
            }
            if (strstr(argv[i + 1], "p") != nullptr) {
                *types |= PBINARY;
            }
            if (strstr(argv[i + 1], "g") != nullptr) {
                *types |= GBINARY;
            }
        } else if (strcmp(argv[i], "--mode") == 0) {
            if (strcmp(argv[i + 1], "r") == 0) {
                *mode = RANDOM;
            } else if (strcmp(argv[i + 1], "u") == 0) {
                *mode = UPW;
            } else if (strcmp(argv[i + 1], "b") == 0) {
                *mode = BCKW;
            }else if (strcmp(argv[i + 1], "c") == 0) {
                *mode = CRCR;
            }
        } else if (strcmp(argv[i], "--amount") == 0) {
            *amount = stoll(argv[i + 1]);
        } else if (strcmp(argv[i], "--raw-path") == 0) {
            ifstream input;
            input.open(argv[i + 1], ios::in);
            if (input) {
                ImportLine(&input, seq);
            }
        }
    }
    if (*types <= 0) {
        *types = AVL | TREE | PBINARY | GBINARY | BINARY;
    }
}

void Exec(bool check, ostream* str, SearchTask<int>* task) {
    vector<double> rel;
    if ((task->GetMethods() & PBINARY) != 0) {
        cout << "How many relations do you have?\n>";
        int num;
        scanf("%d", &num);
        if (num < 0) {
            string er = "number of relations is negative in ";
            er += __PRETTY_FUNCTION__;
            throw invalid_argument(er);
        }
        if (num != 0) {
            cout << "Type pairs of natural numbers separated by spacebar.\n";
        }
        for (int i = 0; i < num; i++) {
            cout << "Please type relation number " << i + 1 << ".\n>";
            int rel1, rel2;
            scanf("%u %u", &rel1, &rel2);
            if (rel2 == 0) {
                string er = "second number in relation is 0 in ";
                er += __PRETTY_FUNCTION__;
                throw invalid_argument(er);
            }
            rel.push_back((double)rel1 / (double)rel2);
        }
    }
    if (*str) {
        task->PrintRaw(str);
    }
    cout << "Now start to input elements for search.\nAfter each new member press Enter.\nWhen you finish, type \"end\" and press Enter.\nMembers can be from " << INT_MIN << " to " << INT_MAX << ".\n";
    string line;
    cout << ">";
    while (getline(cin, line)) {
        if (line.compare("end") == 0) {
            break;
        } else if (line.compare("") == 0) {
            continue;
        }
        task->Search(stoi(line), check, str, rel);
        cout << ">";
    }
}

int main(int argc, const char * argv[]) {
    SearchTask<int>* main_task = nullptr;
    ArraySequence<int> seq;
    ofstream report_file;
    bool check = false;
    int types = 0;
    int mode = 0;
    long long int amount = 0;
    try {
        if (argc == 1) {
            cout << "Try --help for instructions.\n";
        } else if (strcmp(argv[1], "--help") == 0) {
            cout << HELP;
            cout << "Note that all members of sequences must be from ";
            cout << INT_MIN << " to " << INT_MAX << ".\n";
        } else if (strcmp(argv[1], "--manual") == 0) {
            ArgRec(argc, argv, &types, &report_file, &amount, &mode, &seq);
            try {
                main_task = new SearchTask<int>(types);
            } catch (std::bad_alloc& ba) {
                string er = "Bad memory allocation in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
            Exec(check, &report_file, main_task);
        } else if (strcmp(argv[1], "--manual-check") == 0) {
            check = true;
            ArgRec(argc, argv, &types, &report_file, &amount, &mode, &seq);
            try {
                main_task = new SearchTask<int>(types);
            } catch (std::bad_alloc& ba) {
                string er = "Bad memory allocation in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
            Exec(check, &report_file, main_task);
        } else if (strcmp(argv[1], "--auto") == 0) {
            ArgRec(argc, argv, &types, &report_file, &amount, &mode, &seq);
            if (amount <= 0) {
                string er = "amount is zero or negative in ";
                er += __PRETTY_FUNCTION__;
                throw invalid_argument(er);
            }
            try {
                main_task = new SearchTask<int>(amount, mode, types);
            } catch (std::bad_alloc& ba) {
                string er = "Bad memory allocation in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
            Exec(check, &report_file, main_task);
        } else if (strcmp(argv[1], "--auto-check") == 0) {
            check = true;
            ArgRec(argc, argv, &types, &report_file, &amount, &mode, &seq);
            if (amount <= 0) {
                string er = "amount is zero or negative in ";
                er += __PRETTY_FUNCTION__;
                throw invalid_argument(er);
            }
            try {
                main_task = new SearchTask<int>(amount, mode, types);
            } catch (std::bad_alloc& ba) {
                string er = "Bad memory allocation in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
            Exec(check, &report_file, main_task);
        } else if (strcmp(argv[1], "--file") == 0) {
            ArgRec(argc, argv, &types, &report_file, &amount, &mode, &seq);
            if (seq.GetIsEmpty() == 1) {
                string er = "bad file in ";
                er += __PRETTY_FUNCTION__;
                throw invalid_argument(er);
            }
            try {
                main_task = new SearchTask<int>(&seq, types);
            } catch (std::bad_alloc& ba) {
                string er = "Bad memory allocation in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
            Exec(check, &report_file, main_task);
        } else if (strcmp(argv[1], "--file-check") == 0) {
            check = true;
            ArgRec(argc, argv, &types, &report_file, &amount, &mode, &seq);
            if (seq.GetIsEmpty() == 1) {
                string er = "bad file in ";
                er += __PRETTY_FUNCTION__;
                throw invalid_argument(er);
            }
            try {
                main_task = new SearchTask<int>(&seq, types);
            } catch (std::bad_alloc& ba) {
                string er = "Bad memory allocation in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
            Exec(check, &report_file, main_task);
        }else {
            cout << "Command not found. Try --help for instructions.\n";
        }
    } catch (const runtime_error& re) {
        cout << "Runtime error: " << re.what() << endl;
    } catch (const exception& ex) {
        cout << "Error occurred: " << ex.what() << endl;
    } catch (...) {
        cout << "Unknown failure occurred. Possible memory corruption" << endl;
    }
    return 0;
}
